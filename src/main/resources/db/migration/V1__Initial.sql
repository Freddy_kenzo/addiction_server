CREATE TABLE `user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `uuid` VARCHAR(100) NOT NULL,
  `user_id` VARCHAR(100) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NULL,
  `alarm_time` TIME NULL,
  `alarm_on` BIT(1) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `uuid_UNIQUE` (`uuid` ASC),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC));

CREATE TABLE `test` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `test` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `test_UNIQUE` (`test` ASC));

CREATE TABLE `level` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `level` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `level_UNIQUE` (`level` ASC));

CREATE TABLE `test_level` (
  `test_id` INT(11) NOT NULL,
  `level_id` INT(11) NOT NULL,
  `highest_score` DECIMAL(5,2) NOT NULL DEFAULT 0.00,
  `is_unlocked` BIT(1) NOT NULL DEFAULT b'0',
  `score_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`test_id`, `level_id`),
  INDEX `test_level_fk_level_idx` (`level_id` ASC),
  CONSTRAINT `test_level_fk_test`
    FOREIGN KEY (`test_id`)
    REFERENCES `test` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `test_level_fk_level`
    FOREIGN KEY (`level_id`)
    REFERENCES `level` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE TABLE `games` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `test_id` INT(11) NOT NULL,
  `level_id` INT(11) NOT NULL,
  `score` DECIMAL(5,2) NOT NULL DEFAULT 0.00,
  `score_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `game_level_fk_idx` (`level_id` ASC),
  INDEX `game_test_fk_idx` (`test_id` ASC),
  INDEX `game_user_fk_idx` (`user_id` ASC),
  CONSTRAINT `game_level_fk`
    FOREIGN KEY (`level_id`)
    REFERENCES `level` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `game_test_fk`
    FOREIGN KEY (`test_id`)
    REFERENCES `test` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `game_user_fk`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

ALTER TABLE `test_level` 
ADD COLUMN `user_id` INT(11) NOT NULL AFTER `score_date`,
ADD INDEX `test_level_fk_user_idx` (`user_id` ASC);
ALTER TABLE `test_level` 
ADD CONSTRAINT `test_level_fk_user`
  FOREIGN KEY (`user_id`)
  REFERENCES `user` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
