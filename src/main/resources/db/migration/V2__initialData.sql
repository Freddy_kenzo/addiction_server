ALTER TABLE `user` 
ADD COLUMN `access_token` VARCHAR(255) NOT NULL AFTER `alarm_on`,
ADD COLUMN `refresh_token` VARCHAR(45) NOT NULL AFTER `access_token`;

INSERT INTO `level` (`level`) VALUES ('Level 1');
INSERT INTO `level` (`level`) VALUES ('Level 2');
INSERT INTO `level` (`level`) VALUES ('Level 3');
INSERT INTO `level` (`level`) VALUES ('Level 4');
INSERT INTO `level` (`level`) VALUES ('Level 5');
INSERT INTO `level` (`level`) VALUES ('Level 6');
INSERT INTO `level` (`level`) VALUES ('Level 7');
INSERT INTO `level` (`level`) VALUES ('Level 8');
INSERT INTO `level` (`level`) VALUES ('Level 9');

INSERT INTO `test` (`test`, `description`) VALUES ('Addiction', 'Reduce neuron response to what creates the feeling of addition.');
INSERT INTO `test` (`test`, `description`) VALUES ('Aggression', 'Reduce anger levels and improve temper.');
INSERT INTO `test` (`test`, `description`) VALUES ('Alcohol', 'Become more resilient to the desires of one of the greatest additions.');
INSERT INTO `test` (`test`, `description`) VALUES ('Anxiety', 'Strengthen your brain to against anxiety and what causes anxious thoughts.');
INSERT INTO `test` (`test`, `description`) VALUES ('Appetite', 'Over come the daunting temptation of food!');
INSERT INTO `test` (`test`, `description`) VALUES ('Standard', 'Improve memory, focus and concentration.');
