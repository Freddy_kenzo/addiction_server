package sop.freddy.addiction.exception;

import java.util.List;

public class ValidationError {

  private String type;
  private List<ValidationMessage> errors;

  public ValidationError() {

  }

  public ValidationError(String type, List<ValidationMessage> errors) {
    this.type = type;
    this.errors = errors;

  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public List<ValidationMessage> getErrors() {
    return errors;
  }

  public void setErrors(List<ValidationMessage> errors) {
    this.errors = errors;
  }
}


