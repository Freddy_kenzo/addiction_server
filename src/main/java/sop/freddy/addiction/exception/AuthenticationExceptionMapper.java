package sop.freddy.addiction.exception;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationExceptionMapper implements ExceptionMapper<AuthenticationException> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationExceptionMapper.class);

	@Override
	public Response toResponse(AuthenticationException ex) {
		LOGGER.info("AuthenticationExceptionMapper :: toResponse :: " + ex.toString());

		switch (ex.getMessage()) {
		default:
			return Response.status(HttpServletResponse.SC_UNAUTHORIZED).type("application/json").build();
		}
	}
}
