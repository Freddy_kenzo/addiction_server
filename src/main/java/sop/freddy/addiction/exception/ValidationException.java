package sop.freddy.addiction.exception;

import java.util.List;

@SuppressWarnings("serial")
public class ValidationException extends RuntimeException {

	private List<ValidationMessage> errors;

	public ValidationException() {

	}

	public ValidationException(List<ValidationMessage> errors) {
		this.errors = errors;

	}

	public List<ValidationMessage> getErrors() {
		return errors;
	}

	public void setErrors(List<ValidationMessage> errors) {
		this.errors = errors;
	}
}
