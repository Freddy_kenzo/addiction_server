package sop.freddy.addiction.exception;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class AddictionReliefExceptionMapper implements ExceptionMapper<AddictionReliefException> {

	private Logger log = LoggerFactory.getLogger(AddictionReliefExceptionMapper.class);

	@Override
	public Response toResponse(AddictionReliefException ex) {
		log.error("Exception caught in exception mapper: ", ex.getCause());

		switch (ex.getLocalizedMessage()) {
		case "RIGHT_OF_ADMISSION_RESERVED":
			return Response.status(HttpServletResponse.SC_FORBIDDEN).entity("Right of admission reserved.")
					.type("application/json").build();
		case "DUPLICATE_ENTRY":
			return Response.status(HttpServletResponse.SC_CONFLICT).entity("Duplicate entry.")
					.type("application/json").build();
		case "NOT_FOUND":
			return Response.status(HttpServletResponse.SC_CONFLICT).entity("Entry not found in our records.")
					.type("application/json").build();
		case "UNQUALIFIED":
			return Response.status(HttpServletResponse.SC_FORBIDDEN).entity("You don't qualify to execute this action.")
					.type("application/json").build();
		case "BAD_REQUEST":
			return Response.status(HttpServletResponse.SC_BAD_REQUEST).entity("Please verify your inputs.")
					.type("application/json").build();
		default:
			return Response.status(HttpServletResponse.SC_FORBIDDEN).type("application/json").build();
		}

	}
}
