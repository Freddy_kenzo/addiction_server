package sop.freddy.addiction.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.springframework.stereotype.Component;

@Component
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException> {

  public Response toResponse(ValidationException ex) {
    return Response.status(400).entity(toValidationError(ex)).type("application/json").build();
  }

  private ValidationError toValidationError(ValidationException ex) {
    ValidationError error = new ValidationError();
    error.setType("Validation Error");
    error.setErrors(ex.getErrors());
    return error;
  }

}
