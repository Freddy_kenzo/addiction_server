package sop.freddy.addiction.exception;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.springframework.stereotype.Component;

@Component
public class ConstraintViolationMapper implements ExceptionMapper<ConstraintViolationException> {

  public Response toResponse(ConstraintViolationException ex) {
    return Response.status(400).entity(toValidationError(ex)).type("application/json").build();
  }

  private ValidationError toValidationError(ConstraintViolationException ex) {
    ValidationError error = new ValidationError();
    error.setType("Validation Error");
    error.setErrors(toErrors(ex.getConstraintViolations()));
    return error;
  }

  private List<ValidationMessage> toErrors(Set<ConstraintViolation<?>> constraintViolations) {
    return constraintViolations.stream()
        .map(v -> new ValidationMessage(v.getPropertyPath().toString(), v.getMessage(), v.getInvalidValue()))
        .collect(Collectors.toList());

  }
}
