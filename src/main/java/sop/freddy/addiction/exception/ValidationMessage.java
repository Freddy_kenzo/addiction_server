package sop.freddy.addiction.exception;

public class ValidationMessage {

	private String field;
	private String message;
	private String value;

	public ValidationMessage(String field, String message, Object value) {
		this.field = field;
		this.message = message;
		this.value = value != null ? value.toString() : null;
	}

	public String getField() {
		return field;
	}

	public String getMessage() {
		return message;
	}

	public String getValue() {
		return value;
	}

	public void setField(String field) {
		this.field = field;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
