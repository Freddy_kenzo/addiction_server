package sop.freddy.addiction.exception;

@SuppressWarnings("serial")
public class AddictionReliefException extends RuntimeException {

	public AddictionReliefException() {
		super();
	}

	public AddictionReliefException(String message) {
		super(message);
	}

	public AddictionReliefException(String message, Throwable e) {
		super(message, e);
	}

}
