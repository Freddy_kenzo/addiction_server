package sop.freddy.addiction.dao;

import org.springframework.data.repository.CrudRepository;

import sop.freddy.addiction.dao.entity.Level;

public interface LevelDao extends CrudRepository<Level, Long> {

}
