package sop.freddy.addiction.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import sop.freddy.addiction.dao.entity.TestLevel;
import sop.freddy.addiction.dao.entity.TestLevelId;
import sop.freddy.addiction.dao.entity.User;

public interface TestLevelDao extends CrudRepository<TestLevel, TestLevelId> {

	List<TestLevel> findByUser(User user);
}