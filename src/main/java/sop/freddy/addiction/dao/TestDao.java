package sop.freddy.addiction.dao;

import org.springframework.data.repository.CrudRepository;

import sop.freddy.addiction.dao.entity.Test;

public interface TestDao extends CrudRepository<Test, Long> {

}
