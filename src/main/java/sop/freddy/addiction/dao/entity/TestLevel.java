package sop.freddy.addiction.dao.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import org.joda.time.DateTime;

@SuppressWarnings("serial")
@Entity
@Table(name = "test_level")
public class TestLevel implements Serializable {

	@EmbeddedId
    private TestLevelId id;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @MapsId("testId")
    private Test test;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("levelId")
    private Level level;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @MapsId("userId")
    private User user;
	
	@Column(name = "highest_score")
	private Double highestScore;
	
	@Column(name = "is_unlocked")
	private Boolean isUnlocked;
	
	@Column(name = "score_date")
	private DateTime scoreDate;

	public TestLevelId getId() {
		return id;
	}

	public void setId(TestLevelId id) {
		this.id = id;
	}

	public Double getHighestScore() {
		return highestScore;
	}

	public void setHighestScore(Double highestScore) {
		this.highestScore = highestScore;
	}

	public Boolean getIsUnlocked() {
		return isUnlocked;
	}

	public void setIsUnlocked(Boolean isUnlocked) {
		this.isUnlocked = isUnlocked;
	}

	public DateTime getScoreDate() {
		return scoreDate;
	}

	public void setScoreDate(DateTime scoreDate) {
		this.scoreDate = scoreDate;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Test getTest() {
		return test;
	}

	public void setTest(Test test) {
		this.test = test;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}
}
