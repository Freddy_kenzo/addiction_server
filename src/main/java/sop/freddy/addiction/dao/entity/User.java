package sop.freddy.addiction.dao.entity;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

	@Id
	@Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

	@Column(name = "uuid")
    private String uuid;	//Google UUID

	@Column(name = "user_id")
    private String userId;	//Server ID
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "alarm_time")
	private Time alarmTime;
	
	@Column(name = "alarm_on")
	private Boolean alarmOn;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Time getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(Time alarmTime) {
		this.alarmTime = alarmTime;
	}

	public Boolean getAlarmOn() {
		return alarmOn;
	}

	public void setAlarmOn(Boolean alarmOn) {
		this.alarmOn = alarmOn;
	}
}
