package sop.freddy.addiction.dao;

import org.springframework.data.repository.CrudRepository;

import sop.freddy.addiction.dao.entity.User;

public interface UserDao extends CrudRepository<User, Long> {

	public User findByUuid(String uuid);

	public User findByUserId(String userId);
}
