package sop.freddy.addiction.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import sop.freddy.addiction.dao.entity.Game;
import sop.freddy.addiction.dao.entity.User;

public interface GameDao extends CrudRepository<Game, Long> {

	List<Game> findByUser(User user);
}