package sop.freddy.addiction;


import javax.ws.rs.ApplicationPath;

//import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.validation.internal.ValidationExceptionMapper;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import sop.freddy.addiction.exception.AddictionReliefExceptionMapper;
import sop.freddy.addiction.exception.AuthenticationExceptionMapper;
import sop.freddy.addiction.exception.ConstraintViolationMapper;
import sop.freddy.addiction.resource.AuthenticationResource;
//import sop.freddy.addiction.resource.UserResouce;

@EnableScheduling
@Configuration
@ApplicationPath("/addiction")
public class JerseyConfig extends ResourceConfig {

	public JerseyConfig() {
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
		property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
		register(CorsFilters.class);
		register(AuthenticationResource.class);
		register(ConstraintViolationMapper.class);
	    register(ValidationExceptionMapper.class);
	    register(AuthenticationExceptionMapper.class);
	    register(AddictionReliefExceptionMapper.class);
	    register(AuthenticationResource.class);
//	    register(UserResouce.class);
	 }
}