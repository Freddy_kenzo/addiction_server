package sop.freddy.addiction.service;

import java.sql.Time;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;

import sop.freddy.addiction.dao.UserDao;
import sop.freddy.addiction.dao.entity.User;
import sop.freddy.addiction.resource.model.UserModel;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserDao userDao;

	public User saveUser(Payload payload) { 
		
		User user = new User();
		user.setAlarmOn(false);
		user.setAlarmTime(Time.valueOf("00:00:00"));
		user.setEmail(payload.getEmail());
		user.setUuid(payload.getSubject());
		user.setUserId(UUID.nameUUIDFromBytes(payload.getEmail().getBytes()).toString());
		return userDao.save(user);
	}

	public User getUserByUserId(String userId) {
		return userDao.findByUserId(userId);
	}

	public void updateUser(UserModel userModel, User user) {
		user.setAlarmOn(userModel.getAlarmOn());
		user.setAlarmTime(Time.valueOf(userModel.getAlarmTime()));
		userDao.save(user);
	}
}
