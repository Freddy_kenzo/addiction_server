package sop.freddy.addiction.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import sop.freddy.addiction.exception.AddictionReliefException;

@Service
public class TokenService {

	public static final String TOKEN_SECRET = "s4T2zOIWHMM1sxq";

	@Autowired
	private UserService userService;

	public String createToken(String userId) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			String token = JWT.create().withClaim("userId", userId).withClaim("createdAt", new Date()).sign(algorithm);
			return token;
		} catch (JWTCreationException exception) {
			exception.printStackTrace();
			throw new AddictionReliefException("Failed to generate Signing token :: ", exception);
		}
	}

	public String getUserIdFromToken(String token) {
		try {
			Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
			JWTVerifier verifier = JWT.require(algorithm).build();
			DecodedJWT jwt = verifier.verify(token);
			return jwt.getClaim("userId").asString();
		} catch (JWTVerificationException exception) {
			exception.printStackTrace();
			throw new AddictionReliefException("Failed to verify token :: ", exception);
		}
	}

	public boolean isTokenValid(String token, String user) {
		String userId = this.getUserIdFromToken(token);
		if (user != null) {
			return true;
		}
		else
			return userService.getUserByUserId(userId) != null;
	}
}
