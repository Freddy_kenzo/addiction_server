package sop.freddy.addiction.service;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;

import sop.freddy.addiction.dao.UserDao;
import sop.freddy.addiction.dao.entity.User;
import sop.freddy.addiction.exception.AddictionReliefException;

@Component
public class AuthenticationService {
	
	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ScoreService scoreService;

	public User authenticateGoogleUser(String code) throws Exception {
		try {
			
			GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), JacksonFactory.getDefaultInstance())
				    .setAudience(Arrays.asList("1034451689556-o2eqgp1662fn3lfi5b5qih4iqabilipf.apps.googleusercontent.com"))
				    .build();

			GoogleIdToken idToken = verifier.verify(code);

			

			if (idToken == null)
				System.out.println("***********************************************ROOOOROOROOROOR*********************************************");

			Payload payload = idToken.getPayload();

			User user = userDao.findByUuid(payload.getSubject());

			if (user == null) {
				user = userService.saveUser(payload);
				scoreService.initialiseScore(user);
			}

			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw (new AddictionReliefException(
					"Fail to authenticate user. Please make sure User is logged in or code is valid", e));
		}

	}
}
