package sop.freddy.addiction.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sop.freddy.addiction.dao.GameDao;
import sop.freddy.addiction.dao.entity.Game;
import sop.freddy.addiction.dao.entity.User;

@Service
@Transactional
public class GameService {
	
	@Autowired
	private GameDao gameDao;

	public List<Game> getUserGames(User user){
		
		return gameDao.findByUser(user);
	}
}
