package sop.freddy.addiction.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sop.freddy.addiction.dao.GameDao;
import sop.freddy.addiction.dao.LevelDao;
import sop.freddy.addiction.dao.TestDao;
import sop.freddy.addiction.dao.TestLevelDao;
import sop.freddy.addiction.dao.entity.Game;
import sop.freddy.addiction.dao.entity.Level;
import sop.freddy.addiction.dao.entity.Test;
import sop.freddy.addiction.dao.entity.TestLevel;
import sop.freddy.addiction.dao.entity.TestLevelId;
import sop.freddy.addiction.dao.entity.User;
import sop.freddy.addiction.exception.AddictionReliefException;
import sop.freddy.addiction.resource.model.GameModel;
import sop.freddy.addiction.resource.model.RequestModel;
import sop.freddy.addiction.resource.model.TestLevelModel;

@Service
@Transactional
public class ScoreService {

	@Autowired
	private LevelDao levelDao;

	@Autowired
	private TestDao testDao;
	
	@Autowired
	private TestLevelDao testLevelDao;
	
	@Autowired
	private GameDao gameDao;

	public List<TestLevel> initialiseScore(User user) {
		List<Test> tests = (List<Test>) testDao.findAll();
		List<Level> levels = (List<Level>) levelDao.findAll();
		List<TestLevel> testLevels = new ArrayList<>();
		for (Test test : tests) {
			for (Level level : levels) {
				TestLevel testLevel = new TestLevel();

				TestLevelId id = new TestLevelId();
				id.setLevelId(level.getId());
				id.setTestId(test.getId());
				id.setUserId(user.getId());

				testLevel.setId(id);
				testLevel.setHighestScore(0.00);
				testLevel.setTest(test);
				testLevel.setLevel(level);
				testLevel.setScoreDate(DateTime.now());
				
				if (level.getId() == 1)
					testLevel.setIsUnlocked(true);
				else
					testLevel.setIsUnlocked(false);
				testLevel.setUser(user);
				
				testLevels.add(testLevel);
			}
		}
		
		return (List<TestLevel>) testLevelDao.saveAll(testLevels);
	}
	
	public List<TestLevel> getUserTestLevels(User user){
		
		return testLevelDao.findByUser(user);
	}

	public void updateUserScores(RequestModel requestModel, User user) {

		List<TestLevel> testLevels = new ArrayList<>();
		List<Game> games = new ArrayList<>();
		
		for (TestLevelModel testLevelModel : requestModel.getTestLevelModel()) {
			TestLevelId id = new TestLevelId();
			id.setLevelId(testLevelModel.getLevelId());
			id.setTestId(testLevelModel.getTestId());
			id.setUserId(user.getId());
			
			TestLevel testLevel = testLevelDao.findById(id).orElseThrow(()-> new AddictionReliefException("NOT_FOUND"));
			testLevel.setHighestScore(testLevelModel.getHighestScore());
			testLevel.setIsUnlocked(testLevel.getIsUnlocked());
			testLevel.setScoreDate(DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss").parseDateTime(testLevelModel.getScoreDate()));
			testLevels.add(testLevel);
		}
		
		for (GameModel gameModel: requestModel.getGameModel()) {
			Game game = new Game();
			game.setUser(user);
			game.setScore(gameModel.getScore());
			game.setScoreDate(DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss").parseDateTime(gameModel.getScoreDate()));
			game.setLevel(levelDao.findById(gameModel.getLevelId()).orElseThrow(()-> new AddictionReliefException("NOT_FOUND")));
			game.setTest(testDao.findById(gameModel.getTestId()).orElseThrow(()-> new AddictionReliefException("NOT_FOUND")));
		
			games.add(game);
		}
		
		gameDao.saveAll(games);
		testLevelDao.saveAll(testLevels);
	}
}
