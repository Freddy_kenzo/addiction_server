//package sop.freddy.addiction.resource;
//
//import java.util.List;
//
//import javax.security.sasl.AuthenticationException;
//import javax.transaction.Transactional;
//import javax.validation.Valid;
//import javax.ws.rs.Consumes;
//import javax.ws.rs.POST;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import sop.freddy.addiction.dao.entity.Game;
//import sop.freddy.addiction.dao.entity.TestLevel;
//import sop.freddy.addiction.dao.entity.User;
//import sop.freddy.addiction.exception.AddictionReliefException;
//import sop.freddy.addiction.resource.model.RequestModel;
//import sop.freddy.addiction.service.GameService;
//import sop.freddy.addiction.service.ScoreService;
//import sop.freddy.addiction.service.TokenService;
//import sop.freddy.addiction.service.UserService;
//
//@Component
//@Path("user")
//@Transactional
//public class UserResouce {
//
//	@Autowired
//	private UserService userService;
//
//	@Autowired
//	private ScoreService scoreService;
//
//	@Autowired
//	private GameService gameService;
//
//	@Autowired
//	private TokenService tokenService;
//
//	@POST
//	@Path("/{code}")
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public RequestModel authenticateUser(@Valid RequestModel requestModel, @PathParam("code") String code) {
//		try {
//			if (!tokenService.isTokenValid(requestModel.getUserModel().getToken(),
//					requestModel.getUserModel().getUserId()))
//				throw new AuthenticationException();
//
//			// TODO check if access token and refresh token are still valid
//			
//			User user = userService.getUserByUserId(requestModel.getUserModel().getUserId());
//			scoreService.updateUserScores(requestModel, user);
//			userService.updateUser(requestModel.getUserModel(), user);
//
//			List<TestLevel> testLevels = scoreService.getUserTestLevels(user);
//			List<Game> games = gameService.getUserGames(user);
//			return new RequestModel(user, testLevels, games);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			throw (new AddictionReliefException(
//					"Fail to authenticate user. Please make sure User is logged in or code is valid", e));
//		}
//	}
//
//}
