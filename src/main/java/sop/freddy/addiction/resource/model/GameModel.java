package sop.freddy.addiction.resource.model;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.format.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

import sop.freddy.addiction.dao.entity.Game;

public class GameModel {

	@JsonProperty("level_id")
	public Long levelId;
	
	@JsonProperty("test_id")
	public Long testId;
	
	@JsonProperty("level")
	public String level;
	
	@JsonProperty("test")
	public String test;
	
	@JsonProperty("score")
	public Double score;
	
	@JsonProperty("score_date")
	public String scoreDate;

	public Long getLevelId() {
		return levelId;
	}

	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getScoreDate() {
		return scoreDate;
	}

	public void setScoreDate(String scoreDate) {
		this.scoreDate = scoreDate;
	}
	
	public static List<GameModel> toGameModelList(List<Game> games){
		List<GameModel> gameModels = new ArrayList<>();
		
		for (Game game : games) {
			gameModels.add(toGameModel(game));
		}
		
		return gameModels;
	}
	
	public static GameModel toGameModel(Game game) {
		GameModel gameModel = new GameModel();
		gameModel.setLevel(game.getLevel().getLevel());
		gameModel.setLevelId(game.getLevel().getId());
		gameModel.setScore(game.getScore());
		gameModel.setScoreDate(game.getScoreDate().toString(DateTimeFormat.forPattern("yyyy/MM/dd hh:mm:ss")));
		gameModel.setTest(game.getTest().getTest());
		gameModel.setTestId(game.getTest().getId());
		return gameModel;
	}
}
