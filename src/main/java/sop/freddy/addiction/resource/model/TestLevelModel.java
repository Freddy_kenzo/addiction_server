package sop.freddy.addiction.resource.model;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.format.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonProperty;

import sop.freddy.addiction.dao.entity.TestLevel;

public class TestLevelModel {

	@JsonProperty("level_id")
	public Long levelId;

	@JsonProperty("test_id")
	public Long testId;

	@JsonProperty("level")
	public String level;

	@JsonProperty("test")
	public String test;

	@JsonProperty("highest_score")
	public Double highestScore;

	@JsonProperty("is_unlocked")
	public Boolean isUnlocked;

	@JsonProperty("score_date")
	public String scoreDate;

	public Long getLevelId() {
		return levelId;
	}

	public void setLevelId(Long levelId) {
		this.levelId = levelId;
	}

	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public Double getHighestScore() {
		return highestScore;
	}

	public void setHighestScore(Double highestScore) {
		this.highestScore = highestScore;
	}

	public Boolean getIsUnlocked() {
		return isUnlocked;
	}

	public void setIsUnlocked(Boolean isUnlocked) {
		this.isUnlocked = isUnlocked;
	}

	public String getScoreDate() {
		return scoreDate;
	}

	public void setScoreDate(String scoreDate) {
		this.scoreDate = scoreDate;
	}

	public static List<TestLevelModel> toTestLevelModelList(List<TestLevel> testLevels) {
		List<TestLevelModel> testLevelModels = new ArrayList<>();
		
		for (TestLevel testLevel : testLevels) {
			testLevelModels.add(toTestLevelModel(testLevel));
		}
		return testLevelModels;
	}

	public static TestLevelModel toTestLevelModel(TestLevel testLevel) {
		TestLevelModel testLevelModel = new TestLevelModel();
		testLevelModel.setHighestScore(testLevel.getHighestScore());
		testLevelModel.setIsUnlocked(testLevel.getIsUnlocked());
		testLevelModel.setLevel(testLevel.getLevel().getLevel());
		testLevelModel.setLevelId(testLevel.getLevel().getId());
		testLevelModel.setScoreDate(testLevel.getScoreDate().toString(DateTimeFormat.forPattern("yyyy/MM/dd hh:mm:ss")));
		testLevelModel.setTest(testLevel.getTest().getTest());
		testLevelModel.setTestId(testLevel.getTest().getId());
		return testLevelModel;
	}

}
