package sop.freddy.addiction.resource.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import sop.freddy.addiction.dao.entity.Game;
import sop.freddy.addiction.dao.entity.TestLevel;
import sop.freddy.addiction.dao.entity.User;

public class RequestModel {

	@JsonProperty("user_model")
	public UserModel userModel;
	
	@JsonProperty("test_level_model")
	public List<TestLevelModel> testLevelModel;
	
	@JsonProperty("game_model")
	public List<GameModel> gameModel;

	public RequestModel() {
	}

	public RequestModel(User user, List<TestLevel> testLevels, List<Game> games) {
		this.userModel = UserModel.toUserModel(user);
		this.testLevelModel = TestLevelModel.toTestLevelModelList(testLevels);
		this.gameModel = GameModel.toGameModelList(games);
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}

	public List<TestLevelModel> getTestLevelModel() {
		return testLevelModel;
	}

	public void setTestLevelModel(List<TestLevelModel> testLevelModel) {
		this.testLevelModel = testLevelModel;
	}

	public List<GameModel> getGameModel() {
		return gameModel;
	}

	public void setGameModel(List<GameModel> gameModel) {
		this.gameModel = gameModel;
	}
	
	public static RequestModel toRequestModel(User user) {
		RequestModel requestModel = new RequestModel();
		requestModel.setUserModel(UserModel.toUserModel(user));
		return null;
	}
}
