package sop.freddy.addiction.resource.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import sop.freddy.addiction.dao.entity.User;

public class UserModel {

	@JsonProperty("alarm_time")
	public String alarmTime;

	@JsonProperty("user_id")
	public String userId;

	@JsonProperty("alarm_on")
	public Boolean alarmOn;

	public String getAlarmTime() {
		return alarmTime;
	}

	public void setAlarmTime(String alarmTime) {
		this.alarmTime = alarmTime;
	}

	public Boolean getAlarmOn() {
		return alarmOn;
	}

	public void setAlarmOn(Boolean alarmOn) {
		this.alarmOn = alarmOn;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public static UserModel toUserModel(User user) {
		UserModel userModel = new UserModel();
		userModel.setAlarmOn(user.getAlarmOn());
		userModel.setAlarmTime(user.getAlarmTime().toString());
		userModel.setUserId(user.getUserId());
		return userModel;
	}
}
