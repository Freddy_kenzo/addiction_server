package sop.freddy.addiction.resource.model;

public class AddictionReliefResponse {

	private String message;
	private String id;
	private String status;

	public AddictionReliefResponse(boolean success, String message, String id) {
		this.message = message;
		this.id = id;
		this.status = success ? "success" : "failed";
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
