ALTER TABLE `test_level` 
DROP PRIMARY KEY,
ADD PRIMARY KEY (`test_id`, `level_id`, `user_id`);

ALTER TABLE `user` 
CHANGE COLUMN `refresh_token` `refresh_token` VARCHAR(255) NOT NULL ,
ADD COLUMN `token` VARCHAR(255) NOT NULL AFTER `refresh_token`;

